/**
 * Created by instancetype on 9/3/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  mongoose = require('mongoose')
, Schema = mongoose.Schema

, CatSchema = new Schema(
    { name : String
    , age  : Number
    }
  )

module.exports = mongoose.model('Cat', CatSchema)