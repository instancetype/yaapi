/**
 * Created by instancetype on 9/3/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  express = require('express')
, router = express.Router()
, Cat = require('../models/cat')


router.use(function(req, res, next) {
  console.log('Received  %s request for: %s', req.method, req.url)
  next()
})

router.get('/', function(req, res) {
  res.json({ message : 'The API is here' })
})



router.route('/cats')

  .post(function(req, res) {
    var cat = new Cat()
    cat.name = req.body.name
    cat.age = req.body.age
    console.log(req.body)

    cat.save(function(err, data) {
      if (err) return res.send(err)

      res.json(data)
    })
  })

  .get(function(req, res) {
    Cat.find(function(err, cats) {
      if (err) return res.send(err)

      res.json(cats)
    })
  })



router.route('/cats/:id')

  .get(function(req, res) {
    Cat.findById(req.params.id, function(err, cat) {
      if (err) return res.send(err)

      res.json(cat)
    })
  })

  .put(function(req, res) {
    Cat.findById(req.params.id, function(err, cat) {
      if (err) return res.send(err)

      cat.name = req.body.name || cat.name
      cat.age = req.body.age || cat.age

      cat.save(function(err, data) {
        if (err) return res.send(err)

        res.json(data)
      })
    })
  })

  .delete(function(req, res) {
    Cat.remove(
      { _id : req.params.id }
    , function(err, cat) {
        if (err) return res.send(err)

        res.send({})
      }
    )
  })

module.exports = router