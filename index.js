/**
 * Created by instancetype on 9/3/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  express = require('express')
, bodyParser = require('body-parser')
, mongoose = require('mongoose').connect('mongodb://localhost/cats')

const
  routesV1 = require('./app/routesV1/cats')

const
  app = express()

app
  .use(bodyParser.urlencoded({ extended : true }))
  .use(bodyParser.json())
  .use('/api/v1', routesV1)

const
  port = process.env.PORT || 3000

app.listen(port, function() {
  console.log('listening on port %d', port)
})